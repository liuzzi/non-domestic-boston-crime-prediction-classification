package edu.ai.precog.parser;

import java.io.File;

import weka.core.Instances;
import weka.core.converters.ArffSaver;
import weka.core.converters.CSVLoader;

public class CSVToARFF 
{
	
	public static final String INPUT_CSV = CSVGen.OUTPUT_CSV_PATH;
	public static final String INPUT_CSV_NODEM = "res/crime_incident_report_output.csv";
	public static final String OUTPUT_ARFF = "res/output.arff";

	/**
	 * Main method
	 * Loads the CSV file and creates an ARFF file
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception 
	{
		// load CSV
		CSVLoader loader = new CSVLoader();
		loader.setSource(new File(INPUT_CSV_NODEM));
		Instances data = loader.getDataSet();

		// save ARFF
		ArffSaver saver = new ArffSaver();
		saver.setInstances(data);
		saver.setFile(new File(OUTPUT_ARFF));
		saver.setDestination(new File(OUTPUT_ARFF));
		saver.writeBatch();
		System.out.println("Finished.");
	}
}
