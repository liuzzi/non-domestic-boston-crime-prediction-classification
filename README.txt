README.TXT

Artificial Intelligence
Precog Final Project
Non-Domestic Crime Prediction in Boston
------------------------ 
Francesco Liuzzi
Phani Dharmavarapu
Vishal Potti
Ameya Joshi
-------------------------
OVERVIEW:
We used JAVA for all programming.  
The GUI for testing example crime predictions can be run with the binary runPrecog.sh
The project is already built into a jar, but can be rebuilt with ant.  (run 'ant' in the base directory)
Note: the jar file will not run outside of the base directory because we dont package supplemental files into the jar.  So use the script below in the base directory to run the GUI.

TO RUN:
Ex usage on Ubuntu:     ./runPrecog.sh


DIRECTORY CONTENTS:
'src' folder -- sources
'res' folder contains resources such as the original crime data, intermediate revisions, arff versions, our weather data, and csv outputs
'gui' folder contains resources the GUI uses to process the data.
'models' folder contains the one model we are using right now (weka baysian net)


OUTSIDE LIBRARIES
'weka' for machine learning
'simple-json' library to parse json input and output.
